﻿

// This file was automatically generated.
// Do not make changes directly to this file - edit the template instead.
// 
// The following connection settings were used to generate this file
// 
//     Configuration file:     "MqTest\Web.config"
//     Connection String Name: "MqContext"
//     Connection String:      "Server=EIKON11;Database=MqDB;User Id=sa;password=**zapped**;"

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier
// TargetFrameworkVersion = 4.5
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.ModelConfiguration;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace Data.Models
{
    // ************************************************************************
    // Unit of work
    public interface IMqContext : IDisposable
    {
        IDbSet<TbAdjuntoTicket> TbAdjuntoTickets { get; set; } // tbAdjuntoTicket
        IDbSet<TbDepto> TbDeptoes { get; set; } // tbDepto
        IDbSet<TbTicket> TbTickets { get; set; } // tbTicket
        IDbSet<TbTipoProblema> TbTipoProblemas { get; set; } // tbTipoProblema
        IDbSet<TbUsuario> TbUsuarios { get; set; } // tbUsuario
        IDbSet<VwDetalleTickets> VwDetalleTickets { get; set; } // vw_Detalle_Tickets
        IDbSet<VwHistoricoTickets> VwHistoricoTickets { get; set; } // vw_Historico_Tickets

        int SaveChanges();
        
        // Stored Procedures
    }

    // ************************************************************************
    // Database context
    public partial class MqContext : DbContext, IMqContext
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TbAdjuntoTicket> TbAdjuntoTickets { get; set; } // tbAdjuntoTicket

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TbDepto> TbDeptoes { get; set; } // tbDepto

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TbTicket> TbTickets { get; set; } // tbTicket

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TbTipoProblema> TbTipoProblemas { get; set; } // tbTipoProblema

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<TbUsuario> TbUsuarios { get; set; } // tbUsuario

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<VwDetalleTickets> VwDetalleTickets { get; set; } // vw_Detalle_Tickets

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public IDbSet<VwHistoricoTickets> VwHistoricoTickets { get; set; } // vw_Historico_Tickets

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        static MqContext()
        {
            Database.SetInitializer<MqContext>(null);
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MqContext()
            : base("Name=MqContext")
        {
            InitializePartial();
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MqContext(string connectionString) : base(connectionString)
        {
            InitializePartial();
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public MqContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model) : base(connectionString, model)
        {
            InitializePartial();
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new TbAdjuntoTicketConfiguration());
            modelBuilder.Configurations.Add(new TbDeptoConfiguration());
            modelBuilder.Configurations.Add(new TbTicketConfiguration());
            modelBuilder.Configurations.Add(new TbTipoProblemaConfiguration());
            modelBuilder.Configurations.Add(new TbUsuarioConfiguration());
            modelBuilder.Configurations.Add(new VwDetalleTicketsConfiguration());
            modelBuilder.Configurations.Add(new VwHistoricoTicketsConfiguration());

            OnModelCreatingPartial(modelBuilder);
        }

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new TbAdjuntoTicketConfiguration(schema));
            modelBuilder.Configurations.Add(new TbDeptoConfiguration(schema));
            modelBuilder.Configurations.Add(new TbTicketConfiguration(schema));
            modelBuilder.Configurations.Add(new TbTipoProblemaConfiguration(schema));
            modelBuilder.Configurations.Add(new TbUsuarioConfiguration(schema));
            modelBuilder.Configurations.Add(new VwDetalleTicketsConfiguration(schema));
            modelBuilder.Configurations.Add(new VwHistoricoTicketsConfiguration(schema));
            return modelBuilder;
        }

        partial void InitializePartial();
        partial void OnModelCreatingPartial(DbModelBuilder modelBuilder);
        
        // Stored Procedures
    }

    // ************************************************************************
    // Fake Database context
    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class FakeMqContext : IMqContext
    {
        public IDbSet<TbAdjuntoTicket> TbAdjuntoTickets { get; set; }
        public IDbSet<TbDepto> TbDeptoes { get; set; }
        public IDbSet<TbTicket> TbTickets { get; set; }
        public IDbSet<TbTipoProblema> TbTipoProblemas { get; set; }
        public IDbSet<TbUsuario> TbUsuarios { get; set; }
        public IDbSet<VwDetalleTickets> VwDetalleTickets { get; set; }
        public IDbSet<VwHistoricoTickets> VwHistoricoTickets { get; set; }

        public FakeMqContext()
        {
            TbAdjuntoTickets = new FakeDbSet<TbAdjuntoTicket>();
            TbDeptoes = new FakeDbSet<TbDepto>();
            TbTickets = new FakeDbSet<TbTicket>();
            TbTipoProblemas = new FakeDbSet<TbTipoProblema>();
            TbUsuarios = new FakeDbSet<TbUsuario>();
            VwDetalleTickets = new FakeDbSet<VwDetalleTickets>();
            VwHistoricoTickets = new FakeDbSet<VwHistoricoTickets>();
        }

        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose()
        {
            throw new NotImplementedException(); 
        }
        
        // Stored Procedures
    }

    // ************************************************************************
    // Fake DbSet
    [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
    public class FakeDbSet<T> : IDbSet<T> where T : class
    {
        private readonly HashSet<T> _data;

        public FakeDbSet()
        {
            _data = new HashSet<T>();
        }

        public virtual T Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        public T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        public T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        public void Detach(T item)
        {
            _data.Remove(item);
        }

        Type IQueryable.ElementType
        {
            get { return _data.AsQueryable().ElementType; }
        }

        Expression IQueryable.Expression
        {
            get { return _data.AsQueryable().Expression; }
        }

        IQueryProvider IQueryable.Provider
        {
            get { return _data.AsQueryable().Provider; }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        public T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public ObservableCollection<T> Local
        {
            get
            {
                return new ObservableCollection<T>(_data);
            }
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }
    }

    // ************************************************************************
    // POCO classes

    // tbAdjuntoTicket
    public partial class TbAdjuntoTicket
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdAdjunto { get; set; } // idAdjunto (Primary key)

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdTicket { get; set; } // idTicket

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string RutaAdjunto { get; set; } // rutaAdjunto

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public byte[] Adjunto { get; set; } // adjunto

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Extension { get; set; } // extension

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string CreadoPor { get; set; } // creadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ModificadoPor { get; set; } // modificadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? ModificadoEn { get; set; } // modificadoEn
    }

    // tbDepto
    public partial class TbDepto
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdDepto { get; set; } // idDepto (Primary key)

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Descripcion { get; set; } // Descripcion

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string CreadoPor { get; set; } // creadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ModificadoPor { get; set; } // modificadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? ModificadoEn { get; set; } // modificadoEn
    }

    // tbTicket
    public partial class TbTicket
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdTicket { get; set; } // idTicket (Primary key)

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdUsuarioSolicitante { get; set; } // idUsuarioSolicitante

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdDepartamento { get; set; } // idDepartamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? IdTipoProblema { get; set; } // idTipoProblema

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Titulo { get; set; } // titulo

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Descripcion { get; set; } // descripcion

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Estado { get; set; } // estado

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? FechaCerrado { get; set; } // fechaCerrado

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string CreadoPor { get; set; } // creadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ModificadoPor { get; set; } // modificadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? ModificadoEn { get; set; } // modificadoEn
    }

    // tbTipoProblema
    public partial class TbTipoProblema
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdTipoProblema { get; set; } // idTipoProblema (Primary key)

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Descripcion { get; set; } // descripcion

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string CreadoPor { get; set; } // creadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ModificadoPor { get; set; } // modificadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? ModificadoEn { get; set; } // modificadoEn
    }

    // tbUsuario
    public partial class TbUsuario
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdUsuario { get; set; } // idUsuario (Primary key)

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string NombreUsuario { get; set; } // nombreUsuario

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Nombre { get; set; } // nombre

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdDepartamento { get; set; } // idDepartamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public bool EsActivo { get; set; } // esActivo

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Clave { get; set; } // clave

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Perfil { get; set; } // perfil

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string CreadoPor { get; set; } // creadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string ModificadoPor { get; set; } // modificadoPor

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? ModificadoEn { get; set; } // modificadoEn
        
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbUsuario()
        {
            IdDepartamento = 0;
            Perfil = "E";
            InitializePartial();
        }

        partial void InitializePartial();
    }

    // vw_Detalle_Tickets
    public partial class VwDetalleTickets
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Departamento { get; set; } // Departamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdDepartamento { get; set; } // idDepartamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Problema { get; set; } // Problema

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? FechaCerrado { get; set; } // fechaCerrado

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DescEstado { get; set; } // descEstado

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Nombre { get; set; } // nombre

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? Qty { get; set; } // Qty

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? TiempoTotal { get; set; } // tiempoTotal
    }

    // vw_Historico_Tickets
    public partial class VwHistoricoTickets
    {

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int? Qty { get; set; } // Qty

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Departamento { get; set; } // Departamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public int IdDepartamento { get; set; } // idDepartamento

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string Problema { get; set; } // Problema

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? CreadoEn { get; set; } // creadoEn

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public DateTime? FechaCerrado { get; set; } // fechaCerrado

        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public string DescEstado { get; set; } // descEstado
    }


    // ************************************************************************
    // POCO Configuration

    // tbAdjuntoTicket
    internal partial class TbAdjuntoTicketConfiguration : EntityTypeConfiguration<TbAdjuntoTicket>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbAdjuntoTicketConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tbAdjuntoTicket");
            HasKey(x => x.IdAdjunto);

            Property(x => x.IdAdjunto).HasColumnName("idAdjunto").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IdTicket).HasColumnName("idTicket").IsRequired();
            Property(x => x.RutaAdjunto).HasColumnName("rutaAdjunto").IsOptional().HasMaxLength(200);
            Property(x => x.Adjunto).HasColumnName("adjunto").IsOptional();
            Property(x => x.Extension).HasColumnName("extension").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.CreadoPor).HasColumnName("creadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.ModificadoPor).HasColumnName("modificadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.ModificadoEn).HasColumnName("modificadoEn").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // tbDepto
    internal partial class TbDeptoConfiguration : EntityTypeConfiguration<TbDepto>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbDeptoConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tbDepto");
            HasKey(x => x.IdDepto);

            Property(x => x.IdDepto).HasColumnName("idDepto").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Descripcion).HasColumnName("Descripcion").IsRequired().HasMaxLength(75);
            Property(x => x.CreadoPor).HasColumnName("creadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.ModificadoPor).HasColumnName("modificadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.ModificadoEn).HasColumnName("modificadoEn").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // tbTicket
    internal partial class TbTicketConfiguration : EntityTypeConfiguration<TbTicket>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbTicketConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tbTicket");
            HasKey(x => x.IdTicket);

            Property(x => x.IdTicket).HasColumnName("idTicket").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IdUsuarioSolicitante).HasColumnName("idUsuarioSolicitante").IsRequired();
            Property(x => x.IdDepartamento).HasColumnName("idDepartamento").IsRequired();
            Property(x => x.IdTipoProblema).HasColumnName("idTipoProblema").IsOptional();
            Property(x => x.Titulo).HasColumnName("titulo").IsOptional().HasMaxLength(50);
            Property(x => x.Descripcion).HasColumnName("descripcion").IsRequired().HasMaxLength(200);
            Property(x => x.Estado).HasColumnName("estado").IsRequired().IsFixedLength().IsUnicode(false).HasMaxLength(1);
            Property(x => x.FechaCerrado).HasColumnName("fechaCerrado").IsOptional();
            Property(x => x.CreadoPor).HasColumnName("creadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.ModificadoPor).HasColumnName("modificadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.ModificadoEn).HasColumnName("modificadoEn").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // tbTipoProblema
    internal partial class TbTipoProblemaConfiguration : EntityTypeConfiguration<TbTipoProblema>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbTipoProblemaConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tbTipoProblema");
            HasKey(x => x.IdTipoProblema);

            Property(x => x.IdTipoProblema).HasColumnName("idTipoProblema").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Descripcion).HasColumnName("descripcion").IsRequired().HasMaxLength(75);
            Property(x => x.CreadoPor).HasColumnName("creadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.ModificadoPor).HasColumnName("modificadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.ModificadoEn).HasColumnName("modificadoEn").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // tbUsuario
    internal partial class TbUsuarioConfiguration : EntityTypeConfiguration<TbUsuario>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public TbUsuarioConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".tbUsuario");
            HasKey(x => x.IdUsuario);

            Property(x => x.IdUsuario).HasColumnName("idUsuario").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NombreUsuario).HasColumnName("nombreUsuario").IsRequired().HasMaxLength(35);
            Property(x => x.Nombre).HasColumnName("nombre").IsRequired().HasMaxLength(70);
            Property(x => x.IdDepartamento).HasColumnName("idDepartamento").IsRequired();
            Property(x => x.EsActivo).HasColumnName("esActivo").IsRequired();
            Property(x => x.Clave).HasColumnName("clave").IsRequired().HasMaxLength(200);
            Property(x => x.Perfil).HasColumnName("perfil").IsRequired().IsUnicode(false).HasMaxLength(1);
            Property(x => x.CreadoPor).HasColumnName("creadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.ModificadoPor).HasColumnName("modificadoPor").IsOptional().HasMaxLength(35);
            Property(x => x.ModificadoEn).HasColumnName("modificadoEn").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // vw_Detalle_Tickets
    internal partial class VwDetalleTicketsConfiguration : EntityTypeConfiguration<VwDetalleTickets>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public VwDetalleTicketsConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".vw_Detalle_Tickets");
            HasKey(x => new { x.Departamento, x.IdDepartamento, x.Problema, x.Nombre });

            Property(x => x.Departamento).HasColumnName("Departamento").IsRequired().HasMaxLength(75);
            Property(x => x.IdDepartamento).HasColumnName("idDepartamento").IsRequired();
            Property(x => x.Problema).HasColumnName("Problema").IsRequired().HasMaxLength(75);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.FechaCerrado).HasColumnName("fechaCerrado").IsOptional();
            Property(x => x.DescEstado).HasColumnName("descEstado").IsOptional().IsUnicode(false).HasMaxLength(11);
            Property(x => x.Nombre).HasColumnName("nombre").IsRequired().HasMaxLength(70);
            Property(x => x.Qty).HasColumnName("Qty").IsOptional();
            Property(x => x.TiempoTotal).HasColumnName("tiempoTotal").IsOptional();
            InitializePartial();
        }
        partial void InitializePartial();
    }

    // vw_Historico_Tickets
    internal partial class VwHistoricoTicketsConfiguration : EntityTypeConfiguration<VwHistoricoTickets>
    {
        [GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "1.0.0.0")]
        public VwHistoricoTicketsConfiguration(string schema = "dbo")
        {
            ToTable(schema + ".vw_Historico_Tickets");
            HasKey(x => new { x.Departamento, x.IdDepartamento, x.Problema });

            Property(x => x.Qty).HasColumnName("Qty").IsOptional();
            Property(x => x.Departamento).HasColumnName("Departamento").IsRequired().HasMaxLength(75);
            Property(x => x.IdDepartamento).HasColumnName("idDepartamento").IsRequired();
            Property(x => x.Problema).HasColumnName("Problema").IsRequired().HasMaxLength(75);
            Property(x => x.CreadoEn).HasColumnName("creadoEn").IsOptional();
            Property(x => x.FechaCerrado).HasColumnName("fechaCerrado").IsOptional();
            Property(x => x.DescEstado).HasColumnName("descEstado").IsOptional().IsUnicode(false).HasMaxLength(11);
            InitializePartial();
        }
        partial void InitializePartial();
    }


    // ************************************************************************
    // Stored procedure return models

}

