using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Data.Models
{
    [MetadataType(typeof(tbTicketMetadata))]
    public partial class TbTicket
    {
        class tbTicketMetadata
        {
            [HiddenInput]
            public Int32 IdTicket { get; set; }

            [DisplayName("id Usuario Solicitante")]
            [Required]
            public Int32 IdUsuarioSolicitante { get; set; }

            [DisplayName("id Departamento")]
            [Required]
            public Int32 IdDepartamento { get; set; }

            [DisplayName("titulo")]
            [Required]
            [StringLength(50)]
            public String Titulo { get; set; }

            [DisplayName("descripcion")]
            [Required]
            [StringLength(200)]
            public String Descripcion { get; set; }

            [DisplayName("estado")]
            
            [StringLength(1)]
            public String Estado { get; set; }

            [DisplayName("creado Por")]
            [StringLength(35)]
            public String CreadoPor { get; set; }

            [DisplayName("creado En")]
            public DateTime? CreadoEn { get; set; }

            [DisplayName("modificado Por")]
            [StringLength(35)]
            public String ModificadoPor { get; set; }

            [DisplayName("modificado En")]
            public DateTime? ModificadoEn { get; set; }
        }
    }
}
