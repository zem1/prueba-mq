﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
   public class Roles
    {
        public int Rowid { get; set; }
        public string Perfil { get; set; }
        public int IdMenu { get; set; }
        public bool Status { get; set; }
    }
}
