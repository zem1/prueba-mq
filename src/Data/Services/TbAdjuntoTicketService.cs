﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Data.Models;

namespace Data.Services
{
    public interface ITbAdjuntoTicketService
    {
        IEnumerable<TbAdjuntoTicket> GetAll();
        TbAdjuntoTicket GetById(int id);
        void Insert(TbAdjuntoTicket data);
        void Delete(int id);
        void Update(TbAdjuntoTicket data);
        void Save();
    }
    public class TbAdjuntoTicketService : ITbAdjuntoTicketService, IDisposable
    {
        private readonly MqContext _context;

        public TbAdjuntoTicketService(MqContext context)
        {
            _context = context;
        }

        public IEnumerable<TbAdjuntoTicket> GetAll()
        {
            return _context.TbAdjuntoTickets.ToList();
        }

        public TbAdjuntoTicket GetById(int id)
        {
            return _context.TbAdjuntoTickets.Find(id);
        }

        public void Insert(TbAdjuntoTicket data)
        {
            _context.TbAdjuntoTickets.Add(data);
        }

        public void Delete(int id)
        {
            var data = _context.TbAdjuntoTickets.Find(id);
            _context.TbAdjuntoTickets.Remove(data);
        }

        public void Update(TbAdjuntoTicket data)
        {
            _context.Entry(data).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
