﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Data.Models;

namespace Data.Services
{
    public interface ITbTipoProbemaService
    {
        IEnumerable<TbTipoProblema> GetAll();
        TbTipoProblema GetById(int id);
        void Insert(TbTipoProblema data);
        void Delete(int id);
        void Update(TbTipoProblema data);
        void Save();
    }
    public class TbTipoProbemaService : ITbTipoProbemaService, IDisposable
    {
        private readonly MqContext _context;

        public TbTipoProbemaService(MqContext context)
        {
            _context = context;
        }

        public IEnumerable<TbTipoProblema> GetAll()
        {
            return _context.TbTipoProblemas.ToList();
        }

        public TbTipoProblema GetById(int id)
        {
            return _context.TbTipoProblemas.Find(id);
        }

        public void Insert(TbTipoProblema data)
        {
            _context.TbTipoProblemas.Add(data);
        }

        public void Delete(int id)
        {
            var data = _context.TbTipoProblemas.Find(id);
            _context.TbTipoProblemas.Remove(data);
        }

        public void Update(TbTipoProblema data)
        {
            _context.Entry(data).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
