﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Data.Services
{
    public interface ITbTicketService
    {
        IEnumerable<TbTicket> GetAll();
        TbTicket GetById(int id);
        void Insert(TbTicket data);
        void Delete(int id);
        void Update(TbTicket data);
        void Save();
    }
    public class TbTicketService : ITbTicketService, IDisposable
    {
        private readonly MqContext _context;

        public TbTicketService(MqContext context)
        {
            _context = context;
        }

        public IEnumerable<TbTicket> GetAll()
        {
            return _context.TbTickets.ToList();
        }

        public TbTicket GetById(int id)
        {
            return _context.TbTickets.Find(id);
        }

        public void Insert(TbTicket data)
        {
            _context.TbTickets.Add(data);
        }

        public void Delete(int id)
        {
            var data = _context.TbTickets.Find(id);
            _context.TbTickets.Remove(data);
        }

        public void Update(TbTicket data)
        {
            _context.Entry(data).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
