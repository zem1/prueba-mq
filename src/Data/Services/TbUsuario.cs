﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Data.Models;

namespace Data.Services
{
    public interface ITbUsuarioService
    {
        IEnumerable<TbUsuario> GetAll();
        TbUsuario GetById(int id);
        void Insert(TbUsuario data);
        void Delete(int id);
        void Update(TbUsuario data);
        void Save();
    }
    public class TbUsuarioService : ITbUsuarioService, IDisposable
    {
        private readonly MqContext _context;

        public TbUsuarioService(MqContext context)
        {
            _context = context;
        }

        public IEnumerable<TbUsuario> GetAll()
        {
            return _context.TbUsuarios.ToList();
        }

        public TbUsuario GetById(int id)
        {
            return _context.TbUsuarios.Find(id);
        }

        public void Insert(TbUsuario data)
        {
            _context.TbUsuarios.Add(data);
        }

        public void Delete(int id)
        {
            var data = _context.TbUsuarios.Find(id);
            _context.TbUsuarios.Remove(data);
        }

        public void Update(TbUsuario data)
        {
            _context.Entry(data).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
