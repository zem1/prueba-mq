﻿using System.Collections.Generic;
using System.Linq;
using Data.Models;

namespace Data.General
{
    public class Options
    {
        public IEnumerable<NavBar> NavbarItems()
        {
            var menu = new List<NavBar>
            {
                new NavBar
                {
                    Id = 0,
                    NameOption = "Inicio",
                    Controller = "Home",
                    Action = "Index",
                    ImageClass = "glyphicon glyphicon-home",
                    Estatus = true
                },
                new NavBar
                {
                    Id = 1,
                    NameOption = "Usuarios",
                    Controller = "Usuarios",
                    Action = "Index",
                    ImageClass = "glyphicon glyphicon-user",
                    Estatus = false
                },
                new NavBar
                {
                    Id = 2,
                    NameOption = "Nueva Solicitud",
                    Controller = "Tickets",
                    Action = "Index",
                    ImageClass = "glyphicon glyphicon-warning-sign",
                    Estatus = true
                },
                new NavBar
                {
                    Id = 3,
                    NameOption = "Bitácora de solicitudes",
                    Controller = "Tickets",
                    Action = "Solicitudes",
                    ImageClass = "glyphicon glyphicon-list-alt",
                    Estatus = true
                },
               
                new NavBar
                {
                    Id = 4,
                    NameOption = "Informes",
                    Controller = "Informes",
                    Action = "Index",
                    ImageClass = "glyphicon glyphicon-stats",
                    Estatus = true
                }
                
            };

            return menu.ToList();
        }

        public IEnumerable<Roles> RolesOptions()
        {
            var roles = new List<Roles>
            {
                //Administrador
                new Roles {Rowid = 0, Perfil = "A", IdMenu = 0, Status = true},
                new Roles {Rowid = 1, Perfil = "A", IdMenu = 1, Status = true},
                new Roles {Rowid = 2, Perfil = "A", IdMenu = 2, Status = true},
                new Roles {Rowid = 3, Perfil = "A", IdMenu = 3, Status = true},
                new Roles {Rowid = 4, Perfil = "A", IdMenu = 4, Status = true},
                new Roles {Rowid = 5, Perfil = "A", IdMenu = 5, Status = true},
                new Roles {Rowid = 6, Perfil = "A", IdMenu = 6, Status = true},
                new Roles {Rowid = 7, Perfil = "A", IdMenu = 7, Status = true},
                new Roles {Rowid = 8, Perfil = "A", IdMenu = 8, Status = true},
                new Roles {Rowid = 9, Perfil = "A", IdMenu =9, Status = true},
                new Roles {Rowid = 11, Perfil = "A", IdMenu =10, Status = true},
                //Empleados
                new Roles {Rowid = 100, Perfil = "E", IdMenu = 0, Status = false},
                new Roles {Rowid = 101, Perfil = "E", IdMenu = 1, Status = false},
                new Roles {Rowid = 102, Perfil = "E", IdMenu = 2, Status = false},
                new Roles {Rowid = 103, Perfil = "E", IdMenu = 3, Status = false},
                new Roles {Rowid = 104, Perfil = "E", IdMenu = 4, Status = false},
                new Roles {Rowid = 105, Perfil = "E", IdMenu = 5, Status = false},
                new Roles {Rowid = 106, Perfil = "E", IdMenu = 6, Status = false},
                new Roles {Rowid = 107, Perfil = "E", IdMenu = 7, Status = false},
                new Roles {Rowid = 108, Perfil = "E", IdMenu = 8, Status = false},
                new Roles {Rowid = 109, Perfil = "E", IdMenu = 9, Status = false},
            };
            return roles.ToList();
        }

        public IEnumerable<NavBar> ItemsPerRoles(string controller, string action, string perfil)
        {
            var items = NavbarItems();
            var rolesNav = RolesOptions();

            var navbar = items.Where(p => p.Controller == controller).Select(c => { c.Activeli = "active"; return c; }).ToList();

            navbar = (from nav in items
                      join rol in rolesNav on nav.Id equals rol.IdMenu
                      where rol.Perfil == perfil && nav.Estatus && rol.Status
                      select new NavBar
                      {
                          Id = nav.Id,
                          NameOption = nav.NameOption,
                          Controller = nav.Controller,
                          Action = nav.Action,
                          ImageClass = nav.ImageClass,
                          Estatus = nav.Estatus,
                          Activeli = nav.Activeli
                      }).ToList();

            return navbar.ToList();
        }
    }
}
