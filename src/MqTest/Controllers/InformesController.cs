﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Data.Models;
using Data.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace MqTest.Controllers
{
    public class InformesController : Controller
    {
        private readonly ITbTicketService _tbTicket;
        private readonly ITbTipoProbemaService _tipoProbema;
        private readonly ITbUsuarioService _tbUsuario;

        public InformesController()
        {
            var ctx = new MqContext();
            _tbTicket = new TbTicketService(ctx);
            _tipoProbema = new TbTipoProbemaService(ctx);
            _tbUsuario = new TbUsuarioService(ctx);
        }

        //
        // GET: /Informes/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Historico()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            PopulateUsuario();
            return View();
        }

        public ActionResult Listado()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            PopulateUsuario();
            return View();
        }

        public ActionResult TiempoGestion()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            PopulateUsuario();
            return View();
        }

        public ActionResult Cerradas()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            PopulateUsuario();
            return View();
        }

        public ActionResult GetHistorico([DataSourceRequest] DataSourceRequest request, DateTime? fDesde, DateTime? fHasta, bool todas)
        {
            var data = _tbTicket.GetAll();
            if (!todas)
            {
                data = data.Where(x => x.CreadoEn >= fDesde && x.CreadoEn <= fHasta);
            }
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHistoricoChart(DateTime? fDesde, DateTime? fHasta, bool todas)
        {
            var data = GetHistoricoDetalle();
            if (!todas)
            {
                data = data.Where(x => x.CreadoEn >= fDesde && x.CreadoEn <= fHasta);
            }
            return Json(data);
        }

        private void PopulateTipoProblemas()
        {
            var empleados = _tipoProbema.GetAll();
            var data = from r in empleados
                       select new
                       {
                           Text = r.Descripcion,
                           Value = r.IdTipoProblema
                       };
            ViewData["tipoProblemas"] = data;
        }

        private void PopulateUsuario()
        {
            var empleados = _tbUsuario.GetAll();
            var data = from r in empleados
                       select new
                       {
                           Text = r.Nombre,
                           Value = r.IdUsuario
                       };
            ViewData["usuarios"] = data;
        }

        private void PopulateEstado()
        {
            var perfilLst = new List<ListItem>
            {
                new ListItem { Text = "Abierto", Value="A" },
                new ListItem { Text = "Cerrado", Value="C" },
                new ListItem { Text = "Desestimado", Value="D" }
            };
            ViewData["estados"] = perfilLst;
        }

        //Aqui no se utiliza servicio porque es una vista, esto debería ir en una clase General aparte, dentro del proyecto Data.
        private static IEnumerable<VwDetalleTickets> GetHistoricoDetalle()
        {
            using (var dbContext = new MqContext())
            {
                var data = from r in dbContext.VwDetalleTickets
                    select r;
                return data.ToList();
            }
        }

        public ActionResult GetHistoricoClosed([DataSourceRequest] DataSourceRequest request, DateTime? fDesde, DateTime? fHasta, bool todas)
        {
            var data = _tbTicket.GetAll().Where(x => x.FechaCerrado != null);
            if (!todas)
            {
                data = data.Where(x => x.CreadoEn >= fDesde && x.CreadoEn <= fHasta);
            }
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHistoricoChartClosed(DateTime? fDesde, DateTime? fHasta, bool todas)
        {
            var data = GetHistoricoDetalle().Where(x => x.FechaCerrado != null);
            if (!todas)
            {
                data = data.Where(x => x.CreadoEn >= fDesde && x.CreadoEn <= fHasta);
            }
            return Json(data);
        }

        public ActionResult GetHistoricoTiempo([DataSourceRequest] DataSourceRequest request, DateTime? fDesde, DateTime? fHasta, bool todas)
        {
            var data = GetHistoricoDetalle();
            if (!todas)
            {
                data = data.Where(x => x.CreadoEn >= fDesde && x.CreadoEn <= fHasta);
            }
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}
