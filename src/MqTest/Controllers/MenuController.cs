﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data.Models;
using Data.Services;
using MqTest.Models;

namespace MqTest.Controllers
{
    public class MenuController : Controller
    {
        private readonly ITbUsuarioService _usuario;

        public MenuController()
        {
            _usuario = new TbUsuarioService(new MqContext());
        }
        // GET: Navbar
        public ActionResult Navbar(string controller, string action)
        {
            var user = _usuario.GetAll().FirstOrDefault(x => x.NombreUsuario == Cuser.NombreUsuario);
            var perfil = user == null ? "0" : user.Perfil;
            var data = new Data.General.Options();
            var navBar = data.ItemsPerRoles(controller, action, perfil);
            return PartialView("_navbar", navBar);
        }
    }
}