﻿using Data.Models;
using Data.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MqTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace MqTest.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ITbTicketService _tbTicket;
        private readonly ITbAdjuntoTicketService _adjuntoTicket;
        private readonly ITbUsuarioService _tbUsuario;
        private readonly ITbTipoProbemaService _tipoProbema;

        public TicketsController()
        {
            var ctx = new MqContext();
            _tbTicket = new TbTicketService(ctx);
            _adjuntoTicket = new TbAdjuntoTicketService(ctx);
            _tbUsuario = new TbUsuarioService(ctx);
            _tipoProbema = new TbTipoProbemaService(ctx);
        }

        //
        // GET: /Tickets/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Solicitudes/
        public ActionResult Solicitudes()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            return View();
        }

        public ActionResult SolicitudesB()
        {
            PopulateTipoProblemas();
            PopulateEstado();
            return View();
        }

        //
        // POST: /Tickets/Create
        [HttpPost]
        public ActionResult Create(TbTicket values)
        {
            try
            {
                values.CreadoEn = DateTime.Now;
                values.CreadoPor = Cuser.NombreUsuario;
                values.ModificadoEn = new DateTime(1971, 1, 1);
                values.ModificadoPor = "";
                values.IdDepartamento = GetDeptoUsuario(Convert.ToInt32(Cuser.Usuario));
                values.Estado = "A";
                _tbTicket.Insert(values);
                _tbTicket.Save();

                return Json(new { state = "ok" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return View("Error");
            }
        }

        //
        // GET: /Tickets/Edit/5
        public ActionResult Edit(int id)
        {
            var data = _tbTicket.GetById(id);
            return View(data);
        }

        //
        // POST: /Tickets/Edit/5
        [HttpPost]
        public ActionResult Edit(TbTicket values)
        {
            try
            {
                var data = _tbTicket.GetById(values.IdTicket);
                if (data != null)
                {
                    data.Descripcion = values.Descripcion.Trim();
                    data.Estado = values.Estado;
                    data.IdTipoProblema = values.IdTipoProblema;
                    data.Titulo = values.Titulo;
                    if (values.Estado == "C")
                    {
                        data.FechaCerrado = DateTime.Now;
                    }
                    data.ModificadoEn = DateTime.Now;
                    data.ModificadoPor = Cuser.NombreUsuario;

                    _tbTicket.Save();
                }

                return Json(new { state = "ok" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return View();
            }
        }

        public JsonResult GetUsuario(string text)
        {
            var data = _tbUsuario.GetAll();
            if (text.Length > 2)
            {
                data = data.Where(x => x.Nombre.ToLower().Contains(text.ToLower()) ||
                    x.NombreUsuario.ToLower().Contains(text.ToLower()));
            }
            var dataToSend = (from r in data
                              select new
                              {
                                  r.Nombre,
                                  Codigo = r.IdUsuario
                              }).ToList();
            return Json(dataToSend, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTipoProblemas(string text)
        {
            var data = _tipoProbema.GetAll();
            if (text.Length > 2)
            {
                data = data.Where(x => x.Descripcion.ToLower().Contains(text.ToLower()));
            }
            var dataToSend = (from r in data
                              select new
                              {
                                  Nombre = r.Descripcion,
                                  Codigo = r.IdTipoProblema
                              }).ToList();
            return Json(dataToSend, JsonRequestBehavior.AllowGet);
        }

        private void PopulateTipoProblemas()
        {
            var empleados = _tipoProbema.GetAll();
            var data = from r in empleados
                       select new
                       {
                           Text = r.Descripcion,
                           Value = r.IdTipoProblema
                       };
            ViewData["tipoProblemas"] = data;
        }

        private void PopulateEstado()
        {
            var perfilLst = new List<ListItem>
            {
                new ListItem { Text = "Abierto", Value="A" },
                new ListItem { Text = "Cerrado", Value="C" },
                new ListItem { Text = "Desestimado", Value="D" }
            };
            ViewData["estados"] = perfilLst;
        }

        private int GetDeptoUsuario(int idUsuario)
        {
            var data = _tbUsuario.GetById(idUsuario);
            return data.IdDepartamento;
        }

        public ActionResult GetMisSolcitudes([DataSourceRequest] DataSourceRequest request)
        {
            var data = _tbTicket.GetAll().Where(x => x.IdUsuarioSolicitante == Convert.ToInt32(Cuser.Usuario));
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSolcitudes([DataSourceRequest] DataSourceRequest request)
        {
            var data = _tbTicket.GetAll();
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveFile(IEnumerable<HttpPostedFileBase> files)
        {
            if (files == null) return Content("");
            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file.FileName);
                if (fileName != null)
                {
                    var physicalPath = Path.Combine(Server.MapPath("~/Files"), fileName);
                    file.SaveAs(physicalPath);
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult RemoveFile(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                        // TODO: Aquí se deben verificar los permisos de lectura y o escritura.

                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        //
        // GET: /TipoCartas/Upload/5
        public ActionResult Upload(int id)
        {
            var data = new TbAdjuntoTicket { IdTicket = id };
            return View(data);
        }

        [HttpPost]
        public ActionResult Upload(TbAdjuntoTicket values)
        {
            try
            {
                values.CreadoEn = DateTime.Now;
                values.CreadoPor = Cuser.NombreUsuario;
                values.ModificadoEn = new DateTime(1971, 1, 1);
                values.ModificadoPor = "";

                _adjuntoTicket.Insert(values);
                _adjuntoTicket.Save();

                return Json(new { state = "ok" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return View();
            }
        }

        public ActionResult GetAdjuntos([DataSourceRequest] DataSourceRequest request, int idTicket)
        {
            var data =
                _adjuntoTicket.GetAll()
                    .Where(x => x.IdTicket == idTicket);
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public FileResult Download(string fileName)
        {
            var contentType = string.Empty;

            if (fileName.Contains(".pdf"))
            {
                contentType = "application/pdf";
            }
            else if (fileName.Contains(".docx"))
            {
                contentType = "application/docx";
            }
            return new FilePathResult("~/Files/" + fileName, contentType);
        }
    }
}