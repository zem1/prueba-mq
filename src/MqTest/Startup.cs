﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MqTest.Startup))]
namespace MqTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
