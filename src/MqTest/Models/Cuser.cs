﻿using System.Web;

namespace MqTest.Models
{
    public static class Cuser
    {
        public static string Usuario
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["userId"] : "1";
            }
        }
        public static string Nombre
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["Nombre"] : "Miguel Q.";
            }
        }

        public static string NombreUsuario
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["nombreUsuario"] : "mquezada";
            }
        }
        public static string Perfil
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["perfil"] : "A";
            }
        }
        public static string Empresa
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["empresa"] : "0";
            }
        }
        public static string Lenguaje
        {
            get
            {
                return HttpContext.Current.Request.Cookies["usr"] != null ?
                    HttpContext.Current.Request.Cookies["usr"]["lg"] : "0";
            }
        }
    }
}