USE [master]
GO
/****** Object:  Database [MqDB]    Script Date: 06/13/2017 17:50:47 ******/
CREATE DATABASE [MqDB] ON  PRIMARY 
( NAME = N'MqDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\MqDB.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MqDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\MqDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MqDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MqDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MqDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [MqDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [MqDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [MqDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [MqDB] SET ARITHABORT OFF
GO
ALTER DATABASE [MqDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [MqDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [MqDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [MqDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [MqDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [MqDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [MqDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [MqDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [MqDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [MqDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [MqDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [MqDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [MqDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [MqDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [MqDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [MqDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [MqDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [MqDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [MqDB] SET  READ_WRITE
GO
ALTER DATABASE [MqDB] SET RECOVERY FULL
GO
ALTER DATABASE [MqDB] SET  MULTI_USER
GO
ALTER DATABASE [MqDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [MqDB] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'MqDB', N'ON'
GO
USE [MqDB]
GO
/****** Object:  Table [dbo].[tbUsuario]    Script Date: 06/13/2017 17:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbUsuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombreUsuario] [nvarchar](35) NOT NULL,
	[nombre] [nvarchar](70) NOT NULL,
	[idDepartamento] [int] NOT NULL,
	[esActivo] [bit] NOT NULL,
	[clave] [nvarchar](200) NOT NULL,
	[perfil] [varchar](1) NOT NULL,
	[creadoPor] [nvarchar](35) NULL,
	[creadoEn] [datetime] NULL,
	[modificadoPor] [nvarchar](35) NULL,
	[modificadoEn] [datetime] NULL,
 CONSTRAINT [PK_tbUsuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbUsuario] ON
INSERT [dbo].[tbUsuario] ([idUsuario], [nombreUsuario], [nombre], [idDepartamento], [esActivo], [clave], [perfil], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (1, N'mquezada', N'Miguel Quezada', 0, 1, N'MTIzNDU2', N'A', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbUsuario] OFF
/****** Object:  Table [dbo].[tbTipoProblema]    Script Date: 06/13/2017 17:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbTipoProblema](
	[idTipoProblema] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](75) NOT NULL,
	[creadoPor] [nvarchar](35) NULL,
	[creadoEn] [datetime] NULL,
	[modificadoPor] [nvarchar](35) NULL,
	[modificadoEn] [datetime] NULL,
 CONSTRAINT [PK_tbTipoProblema] PRIMARY KEY CLUSTERED 
(
	[idTipoProblema] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbTipoProblema] ON
INSERT [dbo].[tbTipoProblema] ([idTipoProblema], [descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (1, N'Tipo A', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
INSERT [dbo].[tbTipoProblema] ([idTipoProblema], [descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (2, N'Tipo B', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
INSERT [dbo].[tbTipoProblema] ([idTipoProblema], [descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (3, N'Tipo C', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
INSERT [dbo].[tbTipoProblema] ([idTipoProblema], [descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (4, N'Tipo D', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbTipoProblema] OFF
/****** Object:  Table [dbo].[tbTicket]    Script Date: 06/13/2017 17:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbTicket](
	[idTicket] [int] IDENTITY(1,1) NOT NULL,
	[idUsuarioSolicitante] [int] NOT NULL,
	[idDepartamento] [int] NOT NULL,
	[idTipoProblema] [int] NULL,
	[titulo] [nvarchar](50) NULL,
	[descripcion] [nvarchar](200) NOT NULL,
	[estado] [char](1) NOT NULL,
	[fechaCerrado] [datetime] NULL,
	[creadoPor] [nvarchar](35) NULL,
	[creadoEn] [datetime] NULL,
	[modificadoPor] [nvarchar](35) NULL,
	[modificadoEn] [datetime] NULL,
 CONSTRAINT [PK_tbTicket] PRIMARY KEY CLUSTERED 
(
	[idTicket] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbTicket] ON
INSERT [dbo].[tbTicket] ([idTicket], [idUsuarioSolicitante], [idDepartamento], [idTipoProblema], [titulo], [descripcion], [estado], [fechaCerrado], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (1, 1, 1, 2, N'Titulo', N'Descripción', N'D', CAST(0x0000A791008FE8C3 AS DateTime), N'mquezada', CAST(0x0000A791003B3EB7 AS DateTime), N'mquezada', CAST(0x0000A791009234B1 AS DateTime))
INSERT [dbo].[tbTicket] ([idTicket], [idUsuarioSolicitante], [idDepartamento], [idTipoProblema], [titulo], [descripcion], [estado], [fechaCerrado], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (2, 1, 2, 4, N'TItulo 2', N'Descripcion 2', N'A', NULL, N'mquezada', CAST(0x0000A791003BD5E1 AS DateTime), N'', CAST(0x0000654C00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbTicket] OFF
/****** Object:  Table [dbo].[tbDepto]    Script Date: 06/13/2017 17:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDepto](
	[idDepto] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](75) NOT NULL,
	[creadoPor] [nvarchar](35) NULL,
	[creadoEn] [datetime] NULL,
	[modificadoPor] [nvarchar](35) NULL,
	[modificadoEn] [datetime] NULL,
 CONSTRAINT [PK_tbDepto] PRIMARY KEY CLUSTERED 
(
	[idDepto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbDepto] ON
INSERT [dbo].[tbDepto] ([idDepto], [Descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (1, N'Departamento A', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
INSERT [dbo].[tbDepto] ([idDepto], [Descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (2, N'Departamento B', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
INSERT [dbo].[tbDepto] ([idDepto], [Descripcion], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (3, N'Departamento C', N'mquezada', CAST(0x000007B100000000 AS DateTime), NULL, CAST(0x000007B100000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbDepto] OFF
/****** Object:  Table [dbo].[tbAdjuntoTicket]    Script Date: 06/13/2017 17:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAdjuntoTicket](
	[idAdjunto] [int] IDENTITY(1,1) NOT NULL,
	[idTicket] [int] NOT NULL,
	[rutaAdjunto] [nvarchar](200) NULL,
	[adjunto] [varbinary](max) NULL,
	[extension] [varchar](10) NULL,
	[creadoPor] [nvarchar](35) NULL,
	[creadoEn] [datetime] NULL,
	[modificadoPor] [nvarchar](35) NULL,
	[modificadoEn] [datetime] NULL,
 CONSTRAINT [PK_tbAdjuntoTicket] PRIMARY KEY CLUSTERED 
(
	[idAdjunto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbAdjuntoTicket] ON
INSERT [dbo].[tbAdjuntoTicket] ([idAdjunto], [idTicket], [rutaAdjunto], [adjunto], [extension], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (1, 0, N'Examen Tecnico Evaluacion Programadores.pdf', NULL, N'pdf', N'mquezada', CAST(0x0000A791009FF442 AS DateTime), N'', CAST(0x0000654C00000000 AS DateTime))
INSERT [dbo].[tbAdjuntoTicket] ([idAdjunto], [idTicket], [rutaAdjunto], [adjunto], [extension], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (2, 1, N'C.V. Enmanuel Hernandez (1).pdf', NULL, N'pdf', N'mquezada', CAST(0x0000A79100A90E04 AS DateTime), N'', CAST(0x0000654C00000000 AS DateTime))
INSERT [dbo].[tbAdjuntoTicket] ([idAdjunto], [idTicket], [rutaAdjunto], [adjunto], [extension], [creadoPor], [creadoEn], [modificadoPor], [modificadoEn]) VALUES (3, 1, N'ccf.pdf', NULL, N'pdf', N'mquezada', CAST(0x0000A79100A9A969 AS DateTime), N'', CAST(0x0000654C00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tbAdjuntoTicket] OFF
/****** Object:  View [dbo].[vw_Historico_Tickets]    Script Date: 06/13/2017 17:50:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Historico_Tickets]
AS
    SELECT   COUNT(*) AS Qty,
             B.Descripcion AS Departamento,
             A.idDepartamento,
             C.descripcion AS Problema,
             A.creadoEn,
             A.fechaCerrado,
             CASE 
WHEN A.estado = 'A' THEN 'Abierto' 
WHEN A.estado = 'C' THEN 'Cerrado' 
WHEN A.estado = 'D' THEN 'Desestimado' 
END AS descEstado
    FROM     dbo.tbTicket AS A
             INNER JOIN
             dbo.tbDepto AS B
             ON A.idDepartamento = B.idDepto
             INNER JOIN
             dbo.tbTipoProblema AS C
             ON A.idTipoProblema = C.idTipoProblema
    GROUP BY A.idDepartamento, B.Descripcion, C.descripcion,A.creadoEn,A.fechaCerrado,A.estado
GO
/****** Object:  View [dbo].[vw_Detalle_Tickets]    Script Date: 06/13/2017 17:50:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Detalle_Tickets]
AS
    SELECT B.Descripcion AS Departamento,
           A.idDepartamento,
           C.descripcion AS Problema,
           A.creadoEn,
           A.fechaCerrado,
           CASE 
WHEN A.estado = 'A' THEN 'Abierto' 
WHEN A.estado = 'C' THEN 'Cerrado' 
WHEN A.estado = 'D' THEN 'Desestimado' 
END AS descEstado,
           D.nombre,
           (SELECT COUNT(*)
            FROM   tbTicket
            WHERE  idTipoProblema = A.idTipoProblema) AS Qty,
           DATEDIFF(Day, A.creadoEn, A.fechaCerrado) AS tiempoTotal
    FROM   dbo.tbTicket AS A
           INNER JOIN
           dbo.tbDepto AS B
           ON A.idDepartamento = B.idDepto
           INNER JOIN
           dbo.tbTipoProblema AS C
           ON A.idTipoProblema = C.idTipoProblema
           INNER JOIN
           dbo.tbUsuario AS D
           ON A.idUsuarioSolicitante = D.idUsuario;
GO
/****** Object:  Default [DF_tbUsuario_idDepartamento]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbUsuario] ADD  CONSTRAINT [DF_tbUsuario_idDepartamento]  DEFAULT ((0)) FOR [idDepartamento]
GO
/****** Object:  Default [DF_tbUsuario_perfil]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbUsuario] ADD  CONSTRAINT [DF_tbUsuario_perfil]  DEFAULT ('E') FOR [perfil]
GO
/****** Object:  Default [DF_tbUsuario_creadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbUsuario] ADD  CONSTRAINT [DF_tbUsuario_creadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [creadoEn]
GO
/****** Object:  Default [DF_tbUsuario_modificadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbUsuario] ADD  CONSTRAINT [DF_tbUsuario_modificadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [modificadoEn]
GO
/****** Object:  Default [DF_tbTipoProblema_creadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbTipoProblema] ADD  CONSTRAINT [DF_tbTipoProblema_creadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [creadoEn]
GO
/****** Object:  Default [DF_tbTipoProblema_modificadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbTipoProblema] ADD  CONSTRAINT [DF_tbTipoProblema_modificadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [modificadoEn]
GO
/****** Object:  Default [DF_tbTicket_creadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbTicket] ADD  CONSTRAINT [DF_tbTicket_creadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [creadoEn]
GO
/****** Object:  Default [DF_tbTicket_modificadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbTicket] ADD  CONSTRAINT [DF_tbTicket_modificadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [modificadoEn]
GO
/****** Object:  Default [DF_tbDepto_creadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbDepto] ADD  CONSTRAINT [DF_tbDepto_creadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [creadoEn]
GO
/****** Object:  Default [DF_tbDepto_modificadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbDepto] ADD  CONSTRAINT [DF_tbDepto_modificadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [modificadoEn]
GO
/****** Object:  Default [DF_tbAdjuntoTicket_creadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbAdjuntoTicket] ADD  CONSTRAINT [DF_tbAdjuntoTicket_creadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [creadoEn]
GO
/****** Object:  Default [DF_tbAdjuntoTicket_modificadoEn]    Script Date: 06/13/2017 17:50:48 ******/
ALTER TABLE [dbo].[tbAdjuntoTicket] ADD  CONSTRAINT [DF_tbAdjuntoTicket_modificadoEn]  DEFAULT (((1971)-(1))-(1)) FOR [modificadoEn]
GO
